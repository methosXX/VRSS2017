# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/vr2017/student_project/sources/opensg_vrpn.cpp" "/home/vr2017/student_project/build_vr2017-VirtualBox/CMakeFiles/MyProject.dir/sources/opensg_vrpn.cpp.o"
  "/home/vr2017/student_project/sources/solarmanager.cpp" "/home/vr2017/student_project/build_vr2017-VirtualBox/CMakeFiles/MyProject.dir/sources/solarmanager.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ENABLE_CSM_SUPPORT"
  "ENABLE_VRPN_SUPPORT"
  "OSG_WITH_GIF"
  "OSG_WITH_GLUT"
  "OSG_WITH_JPG"
  "OSG_WITH_PNG"
  "OSG_WITH_TIF"
  "_OSG_HAVE_CONFIGURED_H_"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../includes"
  "/home/vr2017/opensg/latest/include"
  "/home/vr2017/opensg/latest/include/OpenSG"
  "/home/vr2017/inVRs/latest/include"
  "/home/vr2017/vrpn/latest/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
