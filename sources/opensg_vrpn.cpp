#include <cstdlib>
#include <cstddef>
#include <cmath>
#include <iostream>
#include <ios>

#include <OpenSG/OSGGLUT.h>
#include <OpenSG/OSGConfig.h>
#include <OpenSG/OSGSimpleGeometry.h>
#include <OpenSG/OSGGLUTWindow.h>
#include <OpenSG/OSGMultiDisplayWindow.h>
#include <OpenSG/OSGComponentTransform.h>

#include <OSGCSM/OSGCAVESceneManager.h>
#include <OSGCSM/OSGCAVEConfig.h>
#include <OSGCSM/appctrl.h>

#include <vrpn_Tracker.h>
#include <vrpn_Button.h>
#include <vrpn_Analog.h>


#include <OpenSG/OSGMaterialGroup.h>
#include <OpenSG/OSGImage.h>
#include <OpenSG/OSGSimpleTexturedMaterial.h>
#include <OpenSG/OSGSceneFileHandler.h>
#include <OpenSG/OSGImageFileHandler.h>
#include <OpenSG/OSGSkyBackground.h>
#include <OpenSG/OSGPointLight.h>

#include <assert.h>

//#include <inVRs/tools/libraries/Skybox/Skybox.h>

#include "solarmanager.h"

#define NDEBUG


OSG_USING_NAMESPACE

OSGCSM::CAVEConfig cfg;
OSGCSM::CAVESceneManager *mgr = nullptr;
vrpn_Tracker_Remote* tracker =  nullptr;
vrpn_Button_Remote* button = nullptr;
vrpn_Analog_Remote* analog = nullptr;

int timeLastFrame = 0;

// right mouse button pressed
bool rmb = false;

int mouse_last_x = 0;
int mouse_last_y = 0;


Vec3f cameraDir;
float cameraYaw = 0;
float cameraPitch = 0;
Quaternion cameraRot(Vec3f(0,0,-1), osgDegree2Rad(0));
Vec3f cameraPos(0,0,0);
float cameraDist = 50.0f;

const float MOUSE_ROTATE_SENSITIVITY = 0.005f;
const float ROTATE_SENSITIVITY = 0.025f;

const Vec3f ASTEROID_STARTPOS(0.0f, 20.0f, 0.0f);
const float ASTEROID_SIZE = 20.0f;

NodeTransitPtr asteroid_transform;
Vec3f asteroid_speed(.0f, .0f, .0f);
const float ASTEROID_PUSH_SPEED = 400.0f;
const float IMPULSE_SCALING_FACTOR = 1.5f;


SolarManager* solarman;
bool startGravity = false;

bool startImpulse = false;
Vec3f wandLastPos(0,0,0);

std::vector<float> wandspeeds;

//Skybox skybox;

void cleanup()
{
	delete mgr;
	delete tracker;
	delete button;
	delete analog;

	delete solarman;
	asteroid_transform = NULL;
}

void print_tracker();

void push_asteroid();
void push_asteroid(float impulse);

void reset();

NodeTransitPtr buildScene()
{
	NodeRecPtr root = Node::create();
	root->setCore(Group::create());

	/*DirectionalLightRecPtr dirLight = DirectionalLight::create();
	dirLight->setDirection(-.5f, -1, 1);
	dirLight->setDiffuse(Color4f(1,1,1,1));
	dirLight->setAmbient(Color4f(0.2f, 0.2f, 0.2f, 1));
	dirLight->setSpecular(Color4f(1,1,1,1));*/

	PointLightRecPtr sunLight = PointLight::create();
	sunLight->setDiffuse(Color4f(1,1,1,1));
	sunLight->setAmbient(Color4f(0.2f, 0.2f, 0.2f, 1));
	sunLight->setSpecular(Color4f(1,1,1,1));

	NodeRecPtr lightroot = makeNodeFor(sunLight);
	root->addChild(lightroot);


	// Create Asteroid
	NodeRecPtr node_asteroid_trans = Node::create();
	ComponentTransformRecPtr ct_asteroid = ComponentTransform::create();
	ct_asteroid->setTranslation(ASTEROID_STARTPOS);
	ct_asteroid->setScale(Vec3f(ASTEROID_SIZE, ASTEROID_SIZE, ASTEROID_SIZE));

	node_asteroid_trans->setCore(ct_asteroid);

	//NodeRecPtr asteroid = makeSphere(4, ASTEROID_SIZE);
	NodeRecPtr asteroid = SceneFileHandler::the()->read("models/asteroid.obj");	

	GeometryRecPtr geo = dynamic_cast<Geometry*>(asteroid->getCore());
	assert(geo != NULL);
	SimpleMaterialRecPtr mat = dynamic_cast<SimpleMaterial*>(geo->getMaterial());
	assert(mat != NULL);
	mat->setAmbient(Color3f(.2f,.2f,.2f));

	lightroot->addChild(node_asteroid_trans);
	node_asteroid_trans->addChild(asteroid);

	asteroid_transform = (NodeTransitPtr)node_asteroid_trans;


	solarman = new SolarManager();
	solarman->generateSolarSystem(NodeTransitPtr(lightroot));

	return NodeTransitPtr(root);
}

void loadBackground(int numWalls)
{
	ImageUnrecPtr           imgFront  = ImageFileHandler::the()->read("starbox1_front5.png");
    TextureObjChunkUnrecPtr texFront  = TextureObjChunk::create();
    texFront->setImage(imgFront);

    ImageUnrecPtr           imgBack   = ImageFileHandler::the()->read("starbox1_back6.png");
    TextureObjChunkUnrecPtr texBack   = TextureObjChunk::create();
    texBack->setImage(imgBack);

    ImageUnrecPtr           imgLeft   = ImageFileHandler::the()->read("starbox1_left2.png");
    TextureObjChunkUnrecPtr texLeft   = TextureObjChunk::create();
    texLeft->setImage(imgLeft);

    ImageUnrecPtr           imgRight  = ImageFileHandler::the()->read("starbox1_right1.png");
    TextureObjChunkUnrecPtr texRight  = TextureObjChunk::create();
    texRight->setImage(imgRight);

    ImageUnrecPtr           imgTop    = ImageFileHandler::the()->read("starbox1_top3.png");
    TextureObjChunkUnrecPtr texTop    = TextureObjChunk::create();
    texTop->setImage(imgTop);

    ImageUnrecPtr           imgBottom = ImageFileHandler::the()->read("starbox1_bottom4.png");
    TextureObjChunkUnrecPtr texBottom = TextureObjChunk::create();
    texBottom->setImage(imgBottom);

    SkyBackgroundUnrecPtr skyBG = SkyBackground::create();
    skyBG->setFrontTexture (texFront);
    skyBG->setBackTexture  (texBack );
    skyBG->setLeftTexture  (texLeft );
    skyBG->setRightTexture (texRight);
    skyBG->setTopTexture   (texTop  );
    skyBG->setBottomTexture(texBottom);

    for(int i = 0; i < numWalls; ++i)
    {
    	mgr->setBackground(i, skyBG);
    }
}

template<typename T>
T scale_tracker2cm(const T& value)
{
	static const float scale = OSGCSM::convert_length(cfg.getUnits(), 1.f, OSGCSM::CAVEConfig::CAVEUnitCentimeters);
	return value * scale;
}

auto head_orientation = Quaternion(Vec3f(0.f, 1.f, 0.f), osgDegree2Rad(0));
auto head_position = Vec3f(0.f, 170.f, 200.f);	// a 1.7m Person 2m in front of the scene

void VRPN_CALLBACK callback_head_tracker(void* userData, const vrpn_TRACKERCB tracker)
{
	head_orientation = Quaternion(tracker.quat[0], tracker.quat[1], tracker.quat[2], tracker.quat[3]);
	head_position = Vec3f(scale_tracker2cm(Vec3d(tracker.pos)));
}

auto wand_orientation = Quaternion();
auto wand_position = Vec3f();
void VRPN_CALLBACK callback_wand_tracker(void* userData, const vrpn_TRACKERCB tracker)
{
	wand_orientation = Quaternion(tracker.quat[0], tracker.quat[1], tracker.quat[2], tracker.quat[3]);
	wand_position = Vec3f(scale_tracker2cm(Vec3d(tracker.pos)));
}

auto analog_values = Vec3f();
void VRPN_CALLBACK callback_analog(void* userData, const vrpn_ANALOGCB analog)
{
	if (analog.num_channel >= 2)
		analog_values = Vec3f(analog.channel[0], 0, -analog.channel[1]);
}

void VRPN_CALLBACK callback_button(void* userData, const vrpn_BUTTONCB button)
{
	if (button.button == 0 && button.state == 1)
	{	
		if(startGravity)
			return;

		wandLastPos = wand_position;
		wandspeeds.clear();
		startImpulse = true;
	}
	else if(button.button == 0 && button.state == 0)
	{
		if(startGravity)
			return;

		startImpulse = false;
		if(wandspeeds.size() < 4)
		{
			std::cout << "Not enough measurements to infer speed.\n";
			return;
		}
		// skip first speed measurement (since its incorrect)
		float speedmax = wandspeeds[1];
		
		for(int i = 2; i < wandspeeds.size(); ++i)
		{
			//std::cout << i << " wandspeed: " << wandspeeds[i];
			if(wandspeeds[i] > speedmax)
				speedmax = wandspeeds[i];
		}
		std::cout << "max speed: " << speedmax << "\n";
		push_asteroid(speedmax * IMPULSE_SCALING_FACTOR);
	}

	else if(button.button == 2 && button.state == 1)
	{
		reset();
	}


}

void InitTracker(OSGCSM::CAVEConfig &cfg)
{
	try
	{
		const char* const vrpn_name = "DTrack@localhost";
		tracker = new vrpn_Tracker_Remote(vrpn_name);
		tracker->shutup = true;
		tracker->register_change_handler(NULL, callback_head_tracker, cfg.getSensorIDHead());
		tracker->register_change_handler(NULL, callback_wand_tracker, cfg.getSensorIDController());
		button = new vrpn_Button_Remote(vrpn_name);
		button->shutup = true;
		button->register_change_handler(nullptr, callback_button);
		analog = new vrpn_Analog_Remote(vrpn_name);
		analog->shutup = true;
		analog->register_change_handler(NULL, callback_analog);
	}
	catch(const std::exception& e) 
	{
		std::cout << "ERROR: " << e.what() << '\n';
		return;
	}
}

void check_tracker()
{
	tracker->mainloop();
	button->mainloop();
	analog->mainloop();
}

void print_tracker()
{
	std::cout << "Head position: " << head_position << " orientation: " << head_orientation << '\n';
	std::cout << "Wand position: " << wand_position << " orientation: " << wand_orientation << '\n';
	std::cout << "Analog: " << analog_values << '\n';
}

void keyboard(unsigned char k, int x, int y)
{
	Real32 ed;
	switch(k)
	{
		case 'q':
		case 27: 
			cleanup();
			exit(EXIT_SUCCESS);
			break;
		case 'e':
			ed = mgr->getEyeSeparation() * .9f;
			std::cout << "Eye distance: " << ed << '\n';
			mgr->setEyeSeparation(ed);
			break;
		case 'E':
			ed = mgr->getEyeSeparation() * 1.1f;
			std::cout << "Eye distance: " << ed << '\n';
			mgr->setEyeSeparation(ed);
			break;
		case 'h':
			cfg.setFollowHead(!cfg.getFollowHead());
			std::cout << "following head: " << std::boolalpha << cfg.getFollowHead() << '\n';
			break;
		case 'i':
			print_tracker();
			break;
		case '8':
			cameraPitch += ROTATE_SENSITIVITY;
			break;
		case '5':
			cameraPitch -= ROTATE_SENSITIVITY;
			break;
		case '4':
			cameraYaw -= ROTATE_SENSITIVITY;
			break;
		case '6':
			cameraYaw += ROTATE_SENSITIVITY;
			break;
		case ' ':
			push_asteroid();
			break;
		case 'r':
			reset();
			break;
		default:
			std::cout << "Key '" << k << "' ignored\n";
	}
}

void push_asteroid()
{
	startGravity = true;
	//asteroid_speed = Vec3f(20.0f, .0f, -20.0f);
	Vec3f cameraDir_norm = cameraDir;
	cameraDir_norm.normalize();
	asteroid_speed = cameraDir_norm * ASTEROID_PUSH_SPEED;
}

void push_asteroid(float impulse)
{
	startGravity = true;
	Vec3f cameraDir_norm = cameraDir;
	cameraDir_norm.normalize();
	asteroid_speed = cameraDir_norm * impulse;
}

void reset()
{
	asteroid_speed = Vec3f(0,0,0);
	startGravity = false;

	ComponentTransformRecPtr ct_asteroid = dynamic_cast<ComponentTransform*>(asteroid_transform->getCore());
	ct_asteroid->setTranslation(ASTEROID_STARTPOS);

	cameraYaw = 0;
	cameraPitch = 0;

}

void haltSimulation()
{
	startGravity = false;
	asteroid_speed = Vec3f(0,0,0);
}

void mouseFunction(int button, int state, int x, int y)
{
	// Right mouse button event
	if(button == GLUT_RIGHT_BUTTON)
	{
		if(state == GLUT_DOWN)
		{
			rmb = true;
			mouse_last_x = x;
			mouse_last_y = y;
		}
		else
		{
			rmb = false;
		}
	}
}

void motionFunction(int x, int y)
{
	
	if(rmb)
	{
		cameraYaw += MOUSE_ROTATE_SENSITIVITY * (mouse_last_x - x);
		cameraPitch += MOUSE_ROTATE_SENSITIVITY * (y - mouse_last_y);

		mouse_last_x = x;
		mouse_last_y = y;
	}
}


void measureImpulse(float timeStep)
{
	Vec3f wandCurrentPos = wand_position;
	wandspeeds.push_back((wandCurrentPos - wandLastPos).length() / timeStep);
	wandLastPos = wandCurrentPos;
}


void update(float timeStep)
{
	// Analog input
	cameraYaw += ROTATE_SENSITIVITY * analog_values.x();
	cameraPitch += ROTATE_SENSITIVITY * analog_values.z();

	if(startImpulse)
		measureImpulse(timeStep);

	// Update asteroid position
	ComponentTransformRecPtr ct_asteroid = dynamic_cast<ComponentTransform*>(asteroid_transform->getCore());

	if(startGravity)
		asteroid_speed += solarman->getAccelerationVector(ct_asteroid->getTranslation(), 1);

	ct_asteroid->setTranslation(ct_asteroid->getTranslation() + asteroid_speed * timeStep);


	Vec3f speed_norm = asteroid_speed;
	speed_norm.normalize();
	cameraRot = Quaternion(Vec3f(0,1,0), cameraYaw);
	cameraRot = cameraRot * Quaternion(Vec3f(1,0,0), cameraPitch);

	// DEBUGGING ONLY! TODO remove
	//head_orientation = cameraRot;

	mgr->setUserTransform(head_position, head_orientation);

	
	// Camera follows asteroid
	// Camera position and rotation are controll by analog stick while aiming
	// and by headtracking while moving
	Vec3f speedNorm = asteroid_speed;
	speedNorm.normalize();
	if(startGravity)
	{
		Quaternion quat_asteroid = Quaternion(Vec3f(0,0,-1), speedNorm);
		Quaternion sum = quat_asteroid.operator * (head_orientation);

		mgr->setRotation(sum);

		mgr->setTranslation(ct_asteroid->getTranslation() - speedNorm * cameraDist + Vec3f(0, -100, 0));
		if(solarman->checkCollision(ct_asteroid->getTranslation(), ASTEROID_SIZE))
		{
			haltSimulation();
		}
		
	}
	else 
	{
		mgr->setRotation(cameraRot);
		mgr->setTranslation(ct_asteroid->getTranslation() - cameraDir + Vec3f(0, -100, 0));
	}

	cameraRot.multVec(Vec3f(0, 0, -cameraDist), cameraDir);	
}

void setupGLUT(int *argc, char *argv[])
{
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_RGB  |GLUT_DEPTH | GLUT_DOUBLE);
	glutCreateWindow("OpenSG CSMDemo with VRPN API");
	glutDisplayFunc([]()
	{
		// black navigation window
		glClear(GL_COLOR_BUFFER_BIT);

		glutSwapBuffers();
	});
	glutReshapeFunc([](int w, int h)
	{
		mgr->resize(w, h);
		glutPostRedisplay();
	});
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouseFunction);
	glutMotionFunc(motionFunction);
	glutIdleFunc([]()
	{

		// Calculate time since lastFrame
		int timeThisFrame = glutGet(GLUT_ELAPSED_TIME);
		int deltaTime = timeThisFrame - timeLastFrame;
		timeLastFrame = timeThisFrame;

		float timeStep = deltaTime / 1000.0;

		check_tracker();

		// Call custom update function
		update(timeStep);

		commitChanges();
		mgr->redraw();
		// the changelist should be cleared - else things could be copied multiple times
		OSG::Thread::getCurrentChangeList()->clear();
	});
}

int main(int argc, char **argv)
{
#if WIN32
	OSG::preloadSharedObject("OSGFileIO");
	OSG::preloadSharedObject("OSGImageFileIO");
#endif
	try
	{
		bool cfgIsSet = false;
		NodeRefPtr scene = nullptr;

		// ChangeList needs to be set for OpenSG 1.4
		ChangeList::setReadWriteDefault();
		osgInit(argc,argv);

		// evaluate intial params
		for(int a=1 ; a<argc ; ++a)
		{
			if( argv[a][0] == '-' )
			{
				if ( strcmp(argv[a],"-f") == 0 ) 
				{
					char* cfgFile = argv[a][2] ? &argv[a][2] : &argv[++a][0];
					if (!cfg.loadFile(cfgFile)) 
					{
						std::cout << "ERROR: could not load config file '" << cfgFile << "'\n";
						return EXIT_FAILURE;
					}
					cfgIsSet = true;
				}
			} else {
				std::cout << "Loading scene file '" << argv[a] << "'\n";
				scene = SceneFileHandler::the()->read(argv[a], NULL);
			}
		}

		// load the CAVE setup config file if it was not loaded already:
		if (!cfgIsSet) 
		{
			const char* const default_config_filename = "config/mono.csm";
			if (!cfg.loadFile(default_config_filename)) 
			{
				std::cout << "ERROR: could not load default config file '" << default_config_filename << "'\n";
				return EXIT_FAILURE;
			}
		}

		cfg.printConfig();

		setupGLUT(&argc, argv);

		InitTracker(cfg);

		MultiDisplayWindowRefPtr mwin = createAppWindow(cfg, cfg.getBroadcastaddress());

		if (!scene) 
			scene = buildScene();
		commitChanges();

		mgr = new OSGCSM::CAVESceneManager(&cfg);
		mgr->setWindow(mwin );
		mgr->setRoot(scene);
		mgr->showAll();
		mgr->getWindow()->init();
		mgr->turnWandOff();
		mgr->setHeadlight(false);
		mgr->setNearClippingPlane(0.1f);
		mgr->setFarClippingPlane(100000);
		mgr->setUserTransform(head_position, head_orientation);
		loadBackground(cfg.getNumActiveWalls());
	}
	catch(const std::exception& e)
	{
		std::cout << "ERROR: " << e.what() << '\n';
		return EXIT_FAILURE;
	}

	glutMainLoop();
}
