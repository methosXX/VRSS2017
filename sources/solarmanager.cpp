#include <OpenSG/OSGMaterialGroup.h>
#include <OpenSG/OSGImage.h>
#include <OpenSG/OSGSimpleTexturedMaterial.h>
#include <OpenSG/OSGSceneFileHandler.h>
#include <OpenSG/OSGPointLight.h>
#include <assert.h>

#include "solarmanager.h"


SolarManager::~SolarManager()
{
	planets.clear();
}

NodeTransitPtr SolarManager::createPlanet(Vec3f position, float scale, int mass, std::string model)
{
	NodeRecPtr node_trans = Node::create();
	ComponentTransformRecPtr ct = ComponentTransform::create();
	ct->setTranslation(position);
	ct->setRotation(Quaternion(Vec3f(0.0f, 0.0f, 1.0f), osgDegree2Rad(0)));
	ct->setScale(Vec3f(scale, scale, scale));
	node_trans->setCore(ct);
	NodeRecPtr planet = SceneFileHandler::the()->read(model.c_str());

	GeometryRecPtr geo = dynamic_cast<Geometry*>(planet->getCore());
	assert(geo != NULL);
	SimpleMaterialRecPtr mat = dynamic_cast<SimpleMaterial*>(geo->getMaterial());
	assert(mat != NULL);
	mat->setAmbient(Color3f(.2f,.2f,.2f));


	node_trans->addChild(planet);

	planet_attributes attr;
	attr.mass = mass;
	attr.position = position;
	attr.radius = scale;

	planets.push_back(attr);

	return (NodeTransitPtr)node_trans;
}

NodeTransitPtr SolarManager::createStar(Vec3f position, float scale, int mass, std::string model)
{
	NodeRecPtr node_trans = Node::create();
	ComponentTransformRecPtr ct = ComponentTransform::create();
	ct->setTranslation(position);
	ct->setRotation(Quaternion(Vec3f(0.0f, 0.0f, 1.0f), osgDegree2Rad(0)));
	ct->setScale(Vec3f(scale, scale, scale));
	node_trans->setCore(ct);
	NodeRecPtr planet = SceneFileHandler::the()->read(model.c_str());

	GeometryRecPtr geo = dynamic_cast<Geometry*>(planet->getCore());
	assert(geo != NULL);
	SimpleMaterialRecPtr mat = dynamic_cast<SimpleMaterial*>(geo->getMaterial());
	assert(mat != NULL);
	//mat->setAmbient(Color3f(.1f,.1f,.1f));
	mat->setLit(false);


	node_trans->addChild(planet);

	planet_attributes attr;
	attr.mass = mass;
	attr.position = position;
	attr.radius = scale;

	planets.push_back(attr);

	return (NodeTransitPtr)node_trans;
}


void SolarManager::generateSolarSystem(NodeTransitPtr root)
{
	NodeRecPtr solarRoot = Node::create();
	solarRoot->setCore(Group::create());
	root->addChild(solarRoot);

	NodeRecPtr star = createStar(Vec3f(.0f,.0f,-5000.0f), 500.0f, 500, "models/sun.obj");

	solarRoot->addChild(star);
	PointLightRecPtr sunLight = dynamic_cast<PointLight*>(root->getCore());
	sunLight->setBeacon(star);

	solarRoot->addChild(createPlanet(Vec3f(2000.0f,.0f,-2000.0f), 150.0f, 150, "models/planet3.obj"));

	solarRoot->addChild(createPlanet(Vec3f(-500.0f,.0f,-8000.0f), 200.0f, 200, "models/planet1.obj"));

	solarRoot->addChild(createPlanet(Vec3f(-2000.0f,.0f,-7000.0f), 120.0f, 120, "models/planet2.obj"));

	solarRoot->addChild(createPlanet(Vec3f(1500.0f,.0f,-4000.0f), 100.0f, 100, "models/planet4.obj"));

	solarRoot->addChild(createPlanet(Vec3f(-250.0f,.0f,-500.0f), 80.0f, 80, "models/earth.obj"));


	/*NodeRecPtr star = createStar(Vec3f(.0f,.0f,-5000.0f), 500.0f, 500, "models/sphere1.obj");

	solarRoot->addChild(star);
	PointLightRecPtr sunLight = dynamic_cast<PointLight*>(root->getCore());
	sunLight->setBeacon(star);

	solarRoot->addChild(createPlanet(Vec3f(2000.0f,.0f,-2000.0f), 150.0f, 150, "models/sphere1.obj"));

	solarRoot->addChild(createPlanet(Vec3f(-500.0f,.0f,-8000.0f), 200.0f, 200, "models/sphere1.obj"));

	solarRoot->addChild(createPlanet(Vec3f(-2000.0f,.0f,-7000.0f), 120.0f, 120, "models/sphere1.obj"));

	solarRoot->addChild(createPlanet(Vec3f(1500.0f,.0f,-4000.0f), 100.0f, 100, "models/sphere1.obj"));

	solarRoot->addChild(createPlanet(Vec3f(-250.0f,.0f,-500.0f), 80.0f, 80, "models/sphere1.obj"));*/

}

Vec3f SolarManager::getAccelerationVector(Vec3f pos, int mass)
{
	Vec3f acceleration(0,0,0);
	for(int i = 0; i < planets.size(); ++i)
	{	
		float force = GRAV_CONST * mass * planets[i].mass / (planets[i].position - pos).squareLength();
		acceleration += (planets[i].position - pos) * force;
	}
	return acceleration;
}

bool SolarManager::checkCollision(Vec3f position, int radius)
{
	for(int i = 0; i < planets.size(); ++i)
	{
		if((planets[i].position - position).length() < radius + planets[i].radius)
		{
			return true;
		}		
	}
	return false;
}