#include <cstdlib>
#include <cstddef>
#include <cmath>
#include <iostream>
#include <ios>

#include <OpenSG/OSGGLUT.h>
#include <OpenSG/OSGSimpleGeometry.h>
#include <OpenSG/OSGComponentTransform.h>

#include <OSGCSM/OSGCAVESceneManager.h>

OSG_USING_NAMESPACE

const float GRAV_CONST = 10.0;

struct planet_attributes
{
	int mass;
	Vec3f position;
	int radius;
	//NodeTransitPtr node;
};

class SolarManager
{
private:
	/// Stores planet attributes for all planets
	std::vector<planet_attributes> planets;
	/// Create single planet to add to solar system
	NodeTransitPtr createPlanet(Vec3f position, float scale, int mass, std::string model);
	/// Create a star to add to solar system
	NodeTransitPtr createStar(Vec3f position, float scale, int mass, std::string model);


public:
	~SolarManager();
	/// Generate solar system and populate planets vector
	void generateSolarSystem(NodeTransitPtr root);
	/// Calculate acceleration from gravity at given location
	/// Newton's law of universal gravity is used for calculating forces (F = (G m1 m2)/r²)
	Vec3f getAccelerationVector(Vec3f pos, int mass);
	/// Simple collision detection for spherical objects
	bool checkCollision(Vec3f position, int radius);
};